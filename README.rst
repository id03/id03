=============
ID03 project
=============

[![build status](https://gitlab.esrf.fr/id03/id03/badges/master/build.svg)](http://id03.gitlab-pages.esrf.fr/id03)
[![coverage report](https://gitlab.esrf.fr/id03/id03/badges/master/coverage.svg)](http://id03.gitlab-pages.esrf.fr/id03/htmlcov)

ID03 software & configuration

Latest documentation from master can be found [here](http://id03.gitlab-pages.esrf.fr/id03)
