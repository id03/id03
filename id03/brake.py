import gevent
from bliss.common.hook import MotionHook
from bliss.common.logtools import log_debug

class Brake(object):
    """
    DESCRIPTION OF WAGO KEYS:

    motor_power : check whether motor power is on
    brake_cable : check whether brake cable is connected
    emergency_button1 : check whether emergency button 1 is NOT pressed
    emergency_button2 : check whether emergency button 2 is NOT pressed
    release_request : user request to release brake
    release_state : interlock relay which check that
      - motor power is on
      - user requested release
      - brake cable connected
      - emergency button not pressed
    motor_limit_pos / motor_limit_neg : interlock relay that connects or disconnects both
      limit switches of the motor. When both limits are disconnected
      motor cannot be moved
    
    """
    wago_key_names = [
        "release_state",
        "release_request",
        "brake_cable",
        "motor_power",
        "motor_limit_pos",
        "motor_limit_neg",
        "emergency_button1",
        "emergency_button2"
    ]

    def __init__(self, name, config):
        self.name = name
        missing = list()
        self.wago = config.get("wago")
        self._interlock_instance = config.get("interlock_instance", 1)
        self._wkeys = dict()
        for name in self.wago_key_names:
            try:
                self._wkeys[name] = config[name]
            except KeyError:
                missing.append(name)
        if missing:
            raise ValueError(f"{self.name} config missing keys {missing}")

    def read_state(self):
        vals = self.wago.get(*self._wkeys.values())
        return dict(zip(self._wkeys, vals))

    def reset(self):
        self.engage()
        self.wago.interlock_reset(self._interlock_instance)

    def engage(self):
        self.wago.set(self._wkeys["release_request"], 0)

    def release(self):
        self.wago.set(self._wkeys["release_request"], 1)
        gevent.sleep(0.1)

    def __info__(self):
        warn = ""
        state = self.read_state()
        if not state["emergency_button1"]:
            warn += "  Emergency button 1 has been pressed\n"
        if not state["emergency_button2"]:
            warn += "  Emergency button 2 has been pressed\n"
        if not state["motor_power"]:
            warn += "  Motor power is OFF\n"
        if not state["brake_cable"]:
            warn += "  Brake cable disconnected\n"
        if state["release_request"] and not state["release_state"]:
            warn += "  Brake release detected but brake engaged!\n"
        if not state["release_request"] and state["release_state"]:
            warn += "  Brake engage requested but brake released!\n"
        if len(warn):
            msg = "WARNING:\n" + warn
            msg += f"hint: you may have to reset interlock with {self.name}.reset()\n\n"
        else:
            msg = "\n"
        released = state["release_state"] and "RELEASED" or "ENGAGED"
        msg += f"Brake release signal : {released}\n"
        limpos = state["motor_limit_pos"]
        limneg = state["motor_limit_neg"]
        if not limpos and not limneg:
            switch = "BOTH ACTIVE prevent motor from moving"
        elif limpos and limneg:
            switch = "NORMAL operation"
        else:
            if not limpos:
                switch = f"WARNING limit POSITIVE active but limit NEGATIVE not active"
            else:
                switch = f"WARNING limit NEGATIVE active but limit POSITIVE not active"
        msg += f"Motor limit switches : {switch}\n"

        return msg
       
class BrakeMotionHook(MotionHook):
    def __init__(self, name, config):
        self.name = name
        self.brake = config["brake"]
        self._scan_flag = False
        super().__init__()

    def pre_move(self, motion_list):
        if self._scan_flag is False:
            log_debug(self, "pre_move brake release")
            self.brake.release()

    def post_move(self, motion_list):
        if self._scan_flag is False:
            log_debug(self, "post_move brake engage")
            self.brake.engage()

    def pre_scan(self, axes_list):
        self._scan_flag = True
        log_debug(self, "pre_scan brake release")
        self.brake.release()

    def post_scan(self, axes_list):
        self._scan_flag = False
        log_debug(self, "post_scan brake engage")
        self.brake.engage()

