import numpy
from bliss.controllers import motor
from bliss.config.settings import SimpleSetting
from bliss.common.utils import object_method

class UndulatorEnergy(motor.CalcController):    
    """ pseudo-motor for undulator energy """

    GAMMA = 11742 # EBS machine parameter
    HC_OVER_E = 12.39842 # conversion wavelength [Angstrom] to energy [keV]
    
    def initialize(self):
        super().initialize()
        self.B0 = self.config.get("B0", float)
        self.lambda0 = self.config.get("lambda0", float)
        self.alpha = self.config.get("alpha", float)
        self.harmonic_setting = self.config.get("harmonic", str)

        self._harmonic = SimpleSetting(self.harmonic_setting, default_value=1.0)

    def initialize_axis(self, axis):
        super().initialize_axis(axis)
        axis.no_offset = True

    @object_method(types_info=("float", "None"))
    def set_harmonic(self, axis, value):
        if (value != int(value)) or (value < 1.0):
            raise ValueError("Undulator harmonic must be a positive integer (typically 1, 3 or 5)")
        self._harmonic.set(value)

    @object_method(types_info=("None", "float"))
    def get_harmonic(self, axis):
        return self._harmonic.get()

    @property
    def harmonic(self):
        return self._harmonic.get()
   
    def calc_from_real(self, positions_dict):
        ''' calculate photon energy for a given harmonic and gap '''
        gap = positions_dict["undulator"]
        k = 0.0934 * self.lambda0 * self.B0 * numpy.exp(-numpy.pi*gap*self.alpha/self.lambda0)
        lambda_calc = self.lambda0 / 4.0e-7 / self.GAMMA**2 * (k*k + 2.0)

        return {"undenergy": self.HC_OVER_E * self.harmonic / lambda_calc}

    def calc_to_real(self, positions_dict):
        ''' calculate gap for a given harmnonic and photon energy '''
        undene = positions_dict["undenergy"]
        # wavelength of the fundamental harmonic
        lambd = self.HC_OVER_E*self.harmonic/undene
        # the square of k
        k_sq = 4.0e-7 * self.GAMMA**2 * lambd/self.lambda0 - 2.0
        if k_sq < 0.0:
            return {"undulator": numpy.nan}
            # raise ValueError(f"{self.name}: Energy {undene} too high for harmonic {self.harmonic}")

        k = numpy.sqrt(k_sq)
        gap = -self.lambda0 / self.alpha / numpy.pi * numpy.log(k / 0.0934 / self.lambda0 / self.B0)       
        return {"undulator": gap}

