
from bliss.controllers.lima.lima_base import Lima
from bliss.common.measurementgroup import get_active

def cam(camera, usebpm=False):
     if isinstance(camera, str):
         camname = camera
     elif isinstance(camera, Lima):
         camname = camera.name
     else:
         raise ValueError("Give a valid camera name or object")

     measgroup = get_active()
     camimage = f"{camname}:image"
     if camimage not in measgroup.available:
         raise ValueError(f"Camera [{camname}] not in active measurement group")

     cambpm = f"{camname}:bpm:"
     hasbpm = False
     for cntname in measgroup.available:
         if cntname.startswith(cambpm):
             hasbpm = True

     for cntname in measgroup.enabled:
         if cntname.endswith(":image"):
             name = cntname.split(":")[0]
             if name != camname:
                 measgroup.disable(name)
         if ":bpm:" in cntname:
             name = cntname.split(":")[0]
             measgroup.disable(f"{name}:bpm:*")

     measgroup.enable(camname)
     if not usebpm:
         if hasbpm:
             measgroup.disable(f"{cambpm}*")
     else:
         if not hasbpm:
             raise ValueError(f"No BPM counters found for camera {camname}")
         else:
             measgroup.enable(f"{cambpm}*")
    

