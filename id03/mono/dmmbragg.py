import numpy
from bliss.controllers import motor

class DMMBragg(motor.CalcController):
    DIST_SECOND_MIRROR=1190
    DIST_FIRST_MIRROR=310
    TOTAL_LENGHT = 310+1190

    def initialize_axis(self, axis):
        super().initialize_axis(axis)
        axis.no_offset = True

    def calc_from_real(self, positions_dict):
        y1 = positions_dict["y1"]
        y2 = positions_dict["y2"]
        #offset = ((y2-y1)/self.TOTAL_LENGHT) * self.DIST_SECOND_MIRROR - y2
        offset = ((self.DIST_FIRST_MIRROR * y2) + (self.DIST_SECOND_MIRROR * y1)) / self.TOTAL_LENGHT
        angle = numpy.arctan((y2-y1)/self.TOTAL_LENGHT) * 180. / numpy.pi
        return {"offset": offset,"angle":angle}

    def calc_to_real(self, positions_dict):
        angle = positions_dict["angle"] * numpy.pi / 180.
        offset = positions_dict["offset"]
        y2 = offset + numpy.tan(angle) * self.DIST_SECOND_MIRROR
        y1 = offset - numpy.tan(angle) * self.DIST_FIRST_MIRROR
        return {"y1":y1,"y2":y2}
