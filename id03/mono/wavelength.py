
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# inspired by EnergyWavelength by A. Beteva and UndulatorEnergy by E. Papillion

import numpy
from bliss.controllers import motor
from bliss.common.axis import CalcAxis as BlissCalcAxis

class CalcAxis(BlissCalcAxis):
    def __info__(self):
        return self.controller.__info__()

class ID03Wavelength(motor.CalcController):
    # conversion from photon energy [keV] to wavelength [AA]
    HC_OVER_E = 12.3984193 

    def initialize(self):
        super().initialize()
        # for Bragg angle below the limit, we use the ML mono energy,
        # above we use the crystal mono energy 
        self.inout_limit = self.config.get("inout_limit", float)

    def initialize_axis(self, axis):
        super().initialize_axis(axis)
        axis.no_offset = True

    def calc_from_real(self, positions_dict):
        inout = positions_dict["inout"]
        energy = numpy.where(inout < self.inout_limit,
                             positions_dict["multilayer_energy"],
                             positions_dict["crystal_energy"])
        # avoid division by zero
        wavelength = numpy.where(energy > 1e-3,
                                 self.HC_OVER_E / energy,
                                 numpy.nan)

        return {"energy": energy, "wavelength": wavelength }

    def calc_to_real(self, positions_dict):
        # not supported, this should be a read-only motor (or a counter???)
        raise RuntimeError("NO motion allowed on energy/wavelength pseudo-motors")

    def __info__(self):
        info_str = "\nENERGY-WAVELENGTH CALCULATION MOTOR:\n"

        _ene_axis = self._tagged["energy"][0]
        _wl_axis = self._tagged["wavelength"][0]
        _inout_axis = self._tagged["inout"][0]
        _crystal_axis = self._tagged["crystal_energy"][0]
        _ml_axis = self._tagged["multilayer_energy"][0]

        info_str += (
            f"             inout axis: {_inout_axis.name} ({_inout_axis.position:10.5f})\n"
        )
        info_str += (
            f" inout_limit: {self.inout_limit:10.5f}"
        )
        if _inout_axis.position < self.inout_limit:
            info_str += " --> using multilayer monochromator\n"
        else:
            info_str += " --> using crystal monochromator\n"
    
        info_str += (
            f" multilayer energy axis: {_ml_axis.name} ({_ml_axis.position:10.5f})\n"
        )
        info_str += (
            f"    crystal energy axis: {_crystal_axis.name} ({_crystal_axis.position:10.5f})\n"
        )
        info_str += "\n"
        info_str += (
            f"            energy axis: {_ene_axis.name} ({_ene_axis.position:10.5f})\n"
        )
        info_str += (
            f"        wavelength axis: {_wl_axis.name} ({_wl_axis.position:10.5f})\n\n"
        )

        return info_str
