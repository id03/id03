'''
    Save a list of motor positions to be able to return to them later

    Example:
      mypositions = SavePositions([obx, oby, obz])
      mypositions.save()
      mypositions.write("mypositions.yml")
      ....
      mypositions.read("mypositions.yml")
      mypositions.restore()

    or:

     with SavePositions([obx, oby, obz]):
         ....

'''
import os
from ruamel.yaml import YAML

from bliss import global_map
from bliss.common import axis
from bliss.shell import standard

def _get_name_and_motor(arg):
    ''' find motor name (string) and object for the argument '''
    if isinstance(arg, axis.Axis):
        #if this is a motor object, then get its name
        motor = arg
        name = motor.name
    elif isinstance(arg, str):
        # we are given the name, find the object
        name = arg
        for motor in global_map.get_axes_iter():
            if motor.name == name:
                break
        else:
            # not found
            raise ValueError(f'{name} is not a known motor')
    else:
        raise TypeError(f"argument {arg} must be motor object or motor name")
    return name, motor


class SavePositions:
    ''' save a list of motor positions, and move back to these when needed '''
    DEFAULT_PATH = '/users/blissadm/local/userconf/savepositions/'

    def __init__(self, *args):
        ''' create SavePositions object. Arguments: motor names '''
        # for backwards compatibilty:
        # if arguments are given as list, use the list
        if (len(args) == 1) and isinstance(args[0], list):
            position_list = args[0]
        else:
            position_list = args
        self.position_list = {}
        for name in position_list:
            self.add(name)

    def add(self, name, value=None) -> None:
        ''' add a motor to the list
            set stored position to value.
            If no value given, and motor exists, keep previous position
            else use current motor position
        '''
        name, motor = _get_name_and_motor(name)

        if value is None:
            if name in self.position_list:
                # do not overwrite previously saved position
                if self.position_list[name][1] is not None:
                    return
            if not motor.disabled:
                value = motor.position
        self.position_list[name] = [motor, value]

    def __add__(self, name):
        self.add(name)
        return self

    def remove(self, name):
        ''' remove a motor from the list '''
        name, motor = _get_name_and_motor(name)
        try:
            del self.position_list[name]
        except KeyError:
            raise KeyError(f"motor {name} was not on the list")

    def __sub__(self, name):
        self.remove(name)
        return self

    def __repr__(self) -> str:
        txt = "SAVEPOSITIONS OBJECT\n\n"
        txt += "Saved positions: \n"
        for name, (_, pos) in self.position_list.items():
            txt += f"  {name}: "
            if pos is None:
                txt += "(no position saved yet)\n"
            else:
                txt += f"{pos:10.5f}\n"
        return txt

    def __info__(self):
        return str(self)

    def save(self, *mots) -> None:
        ''' save the current positions of the motors on the list '''
        if len(mots) == 0:
            mots = self.position_list.keys()
        for i in mots:
            if isinstance(i, axis.Axis):
                key = i.name
                motor = i
                if not key in self.position_list:
                    print(f"New motor {key}, adding to the list")
                    self.add(key)
                    continue
            else:
                key = i
                try:
                    motor, _ = self.position_list[i]
                except KeyError:
                    print(f"New motor {key}, adding to the list")
                    self.add(key)
                    continue
            if motor.disabled:
                position = None
            else:
                position = motor.position
            self.position_list[key] = [motor, position]

    def restore(self, keys=None) -> None:
        ''' move motors to saved positions '''
        move_list = []
        if keys is None:
            keys = self.position_list.keys()

        for key in keys:
            motor, pos = self.position_list[key]
            if pos is not None:
                print(f"  restore {key} to {pos:10.5f}")
                move_list.append(motor)
                move_list.append(pos)

        if len(move_list) == 0:
            print(f"No positions saved, doing nothing")
        else:
            standard.umv(*move_list)

    def __enter__(self):
        self.save()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.restore()

    def _check_fname(self, fname):
        ''' check if file name is OK, and if it does not
        contain a path, add the default path '''
        if not os.path.isabs(fname):
            fname = os.path.join(self.DEFAULT_PATH, fname)
        return fname

    def read(self, fname):
        ''' read positions from file '''
        fname = self._check_fname(fname)
        print(f"Read positions from file '{fname}'.")
        yaml = YAML(typ='safe')
        try:
            with open(fname, 'r') as file:
                data = yaml.load(file)
        except Exception as err:
            print(f"Error reading file '{fname}'.")
            raise err
        if not isinstance(data, dict):
            raise TypeError(f"Error, file '{fname}' does not contain a positions dictionary")

        for key, value in data.items():
            self.add(key, value)

    def write(self, fname):
        ''' write positions to file '''
        fname = self._check_fname(fname)
        write_dict = {name: position
                      for name, (_, position) in self.position_list.items()}
        yaml = YAML(typ='safe')
        yaml.default_flow_style = False
        print(f"Write positions to file '{fname}'.")
        try:
            with open(fname, 'w') as file:
                yaml.dump(write_dict, file)
        except Exception as err:
            print(f'Error writing file "{fname}".')
            raise err
