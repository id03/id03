import numpy

from bliss.controllers.motor import CalcController
from bliss.config.settings import HashObjSetting
from bliss.common.logtools import log_debug_data
from bliss.common.protocols import HasMetadataForScan

from .center_of_rotation import calc_from_real, calc_to_real

class SymHexaCORCalc(CalcController, HasMetadataForScan):

    HexaTranslationTags = ["tx", "ty", "tz"]
    HexaRotationTags = ["rx", "ry", "rz"]
    HexaTags = HexaTranslationTags + HexaRotationTags
    CORTags = ["corx", "cory", "corz"]
    UserTags = ["ux", "uy", "uz"]

    def initialize(self):
        super().initialize()

        self._last_pos = HashObjSetting(f"shexacor:{self.name}")
        self._moving_axes = list()

    @property
    def hexapode(self):
        return self.reals[0].controller

    def scan_metadata(self):
        meta = self.hexapode.protocol().get_metadata()
        meta["@NX_class"] = "NXcollection"
        return meta

    def get_cor_positions(self):
        pos = dict()
        for name in self.CORTags:
            pos[name] = self._last_pos.get(name, 0.)
        return pos

    def get_user_positions(self):
        pos = dict()
        for name in self.UserTags:
            pos[name] = self._last_pos.get(name, 0.)
        return pos

    def _save_cor_positions(self, posdict):
        for (name, value) in posdict.items():
            if name in self.CORTags:
                self._last_pos[name] = value

    def _save_user_positions(self, posdict):
        for (name, value) in posdict.items():
            if name in self.UserTags:
                self._last_pos[name] = value

    def _get_complementary_pseudos_pos_dict(self, axes):
        self._moving_axes_tags = [ self._axis_tag(axis) for axis in axes ]
        return super()._get_complementary_pseudos_pos_dict(axes)

    def _is_array_input(self, posdict):
        posval = posdict[list(posdict.keys())[0]]
        return type(posval)==numpy.ndarray
        
    def calc_from_real(self, user_positions):
        if self._is_array_input(user_positions):
            size = len(pos[user_positions.values()[0]])
            log_debug_data(self, "calc_from_real", f"array of {size} positions")
            pos = dict()
            for name in self.HexaTags:
                pos[f"{name}"] = user_positions[f"hexa{name}"]
            cor = self.get_cor_positions()
            for (name, value) in cor.items():
                pos[name] = numpy.array([value,]*size, numpy.float64)
            usr = self.get_user_positions()
            for (name, value) in usr.items():
                pos[name] = numpy.array([value,]*size, numpy.float64)
        else:
            log_debug_data(self, "calc_from_real", user_positions)
            pos = dict()
            for name in self.HexaTags:
                pos[f"{name}"] = user_positions[f"hexa{name}"]
            pos.update(self.get_cor_positions())
            pos.update(self.get_user_positions())
        return pos

    def calc_to_real(self, dial_positions):
        log_debug_data(self, "calc_to_real moving axes", self._moving_axes_tags)

        moving_real_hexa_translations = False
        for tag in self._moving_axes_tags:
            if tag in self.HexaTranslationTags:
                moving_real_hexa_translations = True

        if moving_real_hexa_translations:
            for tag in self._moving_axes_tags:
                if tag in self.HexaRotationTags + self.CORTags + self.UserTags:
                    raise RuntimeError("Cannot move hexapode translation axes with other pseudos")

            if self._is_array_input(dial_positions):
                # for limits checking : do not save user positions
                real_pos = dict()
                for name in self.HexaTags:
                    real_pos[f"hexa{name}"] = dial_positions[f"{name}"]
            else:
                # motion takes place: update user positions
                calc_pos = calc_from_real(dial_positions)
                self._save_user_positions(calc_pos)

                real_pos = dict()
                for name in self.HexaTags:
                    real_pos[f"hexa{name}"] = dial_positions[f"{name}"]
        else:
            if self._is_array_input(dial_positions):
                # limit checking : do not save cor/user positions

                # transform dict of arrays to list of dict
                # eg: {pseudo:[pos1, pos2]} into [ {pseudo:pos1}, {pseudo:pos2} ]
                size = len(pos[dial_positions.values()[0]])
                log_debug_data(self, "calc_to_real", f"array of {size} positions")

                k = dial_positions.keys()
                v = dial_positions.values()
                dict_list = [dict(zip(k, [x[i] for x in v])) for i in range(size)]

                real_pos = dict()
                for name in self.HexaTags:
                    real_pos[name] = list()
                for dial_pos in dict_list:
                   calc_pos = calc_to_real(dial_pos)
                   for name in self.HexaRotationTags:
                       real_pos[f"hexa{name}"].append(dial_pos[f"{name}"])
                   for name in self.HexaTranslationTags:
                       real_pos[f"hexa{name}"].append(calc_pos[f"{name}"])

            else:
                # motion takes place: update user positions
                log_debug_data(self, "calc_to_real", dial_positions)
                calc_pos = calc_to_real(dial_positions)
                real_pos = dict()
                for name in self.HexaRotationTags:
                    real_pos[f"hexa{name}"] = dial_positions[f"{name}"]
                for name in self.HexaTranslationTags:
                    real_pos[f"hexa{name}"] = calc_pos[f"{name}"]

                self._save_cor_positions(dial_positions)
                self._save_user_positions(dial_positions)

                log_debug_data(self, "calculated", calc_pos)
                log_debug_data(self, "returned", real_pos)

        return real_pos

