import numpy
from bliss.controllers.motor import CalcController
from .calc_chi_phi import calc_from_real, calc_to_real

class SymHexaCalcMotor(CalcController):

    def initialize(self):
        super().initialize()

        err = list()
        # rx :real x   ry: real y
        for tag in ["omega", "chi", "phi", "rx", "ry"]:
             axes = self._tagged.get(tag, [])
             if not len(axes):
                 err.append(f"Missing tag [{tag}]")
             if len(axes) > 1:
                 err.append(f"Multiple axis with [{tag}]")
        if len(err):
             errtxt = ", ".join(err)
             raise ValueError(f"Configuration error in SymHexaCalcMotor: {errtxt}")

    def calc_from_real(self, user_positions):
        calc = calc_from_real(user_positions)
        roll = calc["roll"]
        pitch = calc["pitch"]

        omega = numpy.radians(user_positions["omega"])
        rx = user_positions["rx"]
        ry = user_positions["ry"]

        x = rx * numpy.cos(omega) - ry * numpy.sin(omega)
        y = rx * numpy.sin(omega) + ry * numpy.cos(omega)

        return dict(roll=roll, pitch=pitch, x=x, y=y)

    def calc_to_real(self, dial_positions):
        calc = calc_to_real(dial_positions)
        phi = calc["phi"]
        chi = calc["chi"]

        omega = numpy.radians(dial_positions["omega"])
        x = dial_positions["x"]
        y = dial_positions["y"]

        rx = x * numpy.cos(omega) + y * numpy.sin(omega)
        ry = -x * numpy.sin(omega) + y * numpy.cos(omega)

        return dict(phi=phi, chi=chi, rx=rx, ry=ry)


