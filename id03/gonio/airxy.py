import gevent
import time
from bliss.common.hook import MotionHook

class GonioAirXY(object):
    def __init__(self, name, config):
        self.name = name
        self.wago = config.get("wago")
        self.cmdkey = config.get("command")
        self.motors = config.get("motors")

    def on(self):
        self.wago.set(self.cmdkey, 1)
        started = time.time()
        has_limits = True
        while has_limits:
           gevent.sleep(0.2)
           has_limits = False
           for mot in self.motors:
               state = mot.hw_state
               if "LIMPOS" in state and "LIMNEG" in state:
                   has_limits = True
           if has_limits and time.time()-started > 2.:
               self.off()
               raise RuntimeError("Motors limits still ON after 2 sec. Give up and set air OFF.")

    def off(self):
        self.wago.set(self.cmdkey, 0)

    def is_on(self):
        return self.wago.get(self.cmdkey) == 1

    def __info__(self):
        valstr = self.is_on() and "ON" or "OFF"
        info = f"Gonio XY air is {valstr}"
        return info

    def __enter__(self):
        self.on()
        return self

    def __exit__(self, *args):
        self.off()


class GonioAirXYMotionHook(MotionHook):
    def __init__(self, name, config):
        self.air = config.get("control")
        super().__init__()

    def pre_move(self, motion_list):
        if not self.air.is_on():
            raise RuntimeError("Cannot move corx or corz motors without air")

