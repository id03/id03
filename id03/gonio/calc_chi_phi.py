'''
Goniometer calculations:

Transformation from hardware angles to roll/pitch/yaw
'''
import sys
import numpy as np
from random import uniform

##############################################################

def calc_from_real(pos: dict) -> dict:
    ''' transform from goniometer angles to pitch/roll/yaw '''
    phi = np.radians(pos["phi"])
    chi = np.radians(pos["chi"])
    omega = np.radians(pos["omega"])

    s_phi = np.sin(phi)
    c_phi = np.cos(phi)

    s_chi = np.sin(chi)
    c_chi = np.cos(chi)

    s_om = np.sin(omega)
    c_om = np.cos(omega)

    psi = np.arctan2(c_chi * s_om,
                     c_phi * c_om + s_phi * s_chi * s_om)

    theta = np.arctan2(s_phi * c_om - c_phi * s_chi * s_om,
                       np.sqrt(
                           (c_phi * c_om + s_phi * s_chi * s_om)**2
                           + (c_chi * s_om)**2
                           ))
    rho = np.arctan2(c_phi * s_chi * c_om + s_phi * s_om,
                     c_phi * c_chi)

    return {"roll": np.degrees(rho),
            "pitch": np.degrees(theta),
            "yaw": np.degrees(psi)
           }

##############################################################

def calc_to_real(pos: dict) -> dict:
    ''' transform from pitch/roll to goniometer angles '''
    rho = np.radians(pos["roll"])
    theta = np.radians(pos["pitch"])
    omega = np.radians(pos["omega"])

    s_rho = np.sin(rho)
    c_rho = np.cos(rho)

    s_th = np.sin(theta)
    c_th = np.cos(theta)

    s_om = np.sin(omega)
    c_om = np.cos(omega)

    psi = np.arctan2(c_rho * s_om,
                     c_th * c_om - s_th * s_rho * s_om)

    phi = np.arctan2(c_om * s_th + c_th * s_rho * s_om,
                     np.sqrt(
                         (c_rho * s_om)**2
                         +(c_th * c_om - s_th * s_rho * s_om)**2
                         )
                     )

    chi = np.arctan2(c_th * c_om *s_rho - s_th * s_om,
                     c_th * c_rho)

    return {"phi": np.degrees(phi),
            "chi": np.degrees(chi),
            "yaw": np.degrees(psi)
           }

##############################################################
# rotation matrices

def rot_x(alpha: float) -> np.ndarray:
    ''' rotation about x axis. alpha in degrees '''
    alpha = np.radians(alpha)
    s_al = np.sin(alpha)
    c_al = np.cos(alpha)
    return np.array(((1., 0., 0.),
                     (0., c_al, -s_al),
                     (0., s_al, c_al)))


def rot_y(alpha: float) -> np.ndarray:
    ''' rotation about y axis. alpha in degrees '''
    alpha = np.radians(alpha)
    s_al = np.sin(alpha)
    c_al = np.cos(alpha)
    return np.array(((c_al, 0., s_al),
                     (0., 1., 0.),
                     (-s_al, 0., c_al)))

def rot_z(alpha: float) -> np.ndarray:
    ''' rotation about z axis. alpha in degrees '''
    alpha = np.radians(alpha)
    s_al = np.sin(alpha)
    c_al = np.cos(alpha)
    return np.array(((c_al, -s_al, 0.),
                     (s_al, c_al, 0.),
                     (0., 0., 1.)))

##############################################################


def check_calc(real_positions: dict, verbose: bool = False) -> bool:
    ''' test function using random angles '''

    # rotation matrix corresponding to the real goniometer angles
    rot_real = rot_z(real_positions['omega']).dot(
        rot_x(real_positions['chi']).dot(
            rot_y(-real_positions['phi'])))

    # calculate roll, pitch and yaw
    calc_positions = calc_from_real(real_positions)

    # rotation matrix corresponding to roll/pitch/yaw
    rot_calc = rot_x(calc_positions['roll']).dot(
        rot_y(-calc_positions['pitch']).dot(
            rot_z(calc_positions['yaw'])))

    # back-transform calculated angles to check that we get the same angles
    calc_positions["omega"] = real_positions["omega"]
    check_positions = calc_to_real(calc_positions)

    # different between "real" and "calc" rotation matrices
    diff = np.sum((rot_real - rot_calc)**2)

    if verbose:
        print("real positions")
        print(f"   omega = {real_positions['omega']:7.4f}")
        print(f"   phi   = {real_positions['phi']:7.4f} --> {check_positions['phi']:7.4f}")
        print(f"   chi   = {real_positions['chi']:7.4f} --> {check_positions['chi']:7.4f}")

        print("calc positions")
        print(f"   yaw   = {calc_positions['yaw']:7.4f} --> {check_positions['yaw']:7.4f}")
        print(f"   roll  = {calc_positions['roll']:7.4f}")
        print(f"   pitch = {calc_positions['pitch']:7.4f}")

        print(f"difference: {diff:.2e}")

    # check for differences between original "real"
    # and back-and-forth calculated angles
    if np.abs(real_positions['phi'] - check_positions['phi']) > 1e-10:
        raise ValueError("phi != check_phi")

    if np.abs(real_positions['chi'] - check_positions['chi']) > 1e-10:
        raise ValueError("chi != check_chi")

    if np.abs(calc_positions['yaw'] - check_positions['yaw']) > 1e-10:
        raise ValueError("yaw != check_yaw")

    # check if the "real" and "calc" rotation matrices are the same
    if diff > 1e-20:
        raise ValueError("Difference of rotation matrices > 1e-10!")

    return True

##############################################################

def test():
    '''run standalone '''
    # run 1000 tests with random angles
    print("Performing tests on random angles")
    for idx in range(1000):
        omega = uniform(-180., 180.)
        chi = uniform(-90., 90.)
        phi = uniform(-90., 90.)
        real_positions = {
            "chi": chi,
            "phi": phi,
            "omega": omega
            }
        check_calc(real_positions)
    print(f"{idx+1} tests done successfully")

##############################################################

if __name__ == "__main__":
    if len(sys.argv) not in (1, 4) or sys.argv[0].lower().startswith("h"):
        print(f"Usage: {sys.argv[0]} --> perform tests")
        print(f"       {sys.argv[0]} omaga phi chi --> perform one calc")
        sys.exit(0)

    if len(sys.argv) == 1:
        test()
    else:
        omega = float(sys.argv[1])
        phi = float(sys.argv[2])
        chi = float(sys.argv[3])
        check_calc({"omega": omega, "phi": phi, "chi": chi}, verbose=True)
