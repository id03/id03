'''
Center of rotation calculations for Symetrie hexapod

user and object origin of the hexapod controller should
be set to (0,0,0, 0,0,0), *especially* the rotations

Motor names:

User translations:
- ux
- uy
- uz

Center of rotation positions
- corx
- cory
- corz

Hexapod translations
- tx
- ty
- tz

Hexapod rotations
- rx
- ry
- rz
'''

import numpy as np

# rotation matrices:

def rotx(alpha: float) -> np.ndarray:
    ''' rotation about x, angle in degrees '''

    al = np.radians(alpha)
    s_a = np.sin(al)
    c_a = np.cos(al)
    
    return np.array(
        [ [1., 0., 0.],
          [0., c_a, -s_a],
          [0., s_a, c_a]
        ]
    )


def roty(alpha: float) -> np.ndarray:
    ''' rotation about y, angle in degrees '''

    al = np.radians(alpha)
    s_a = np.sin(al)
    c_a = np.cos(al)
    
    return np.array(
        [ [c_a, 0., s_a],
          [0., 1., 0.],
          [-s_a, 0., c_a]
        ]
    )


def rotz(alpha: float) -> np.ndarray:
    ''' rotation about z, angle in degrees '''

    al = np.radians(alpha)
    s_a = np.sin(al)
    c_a = np.cos(al)
    
    return np.array(
        [ [c_a, -s_a, 0.],
          [s_a, c_a,  0.],
          [0., 0., 1.]
        ]
    )

##############################################################

def calc_from_real(pos: dict) -> dict:
    ''' calculate user coordinates
    from hexapod positions and center of rotation positions

    Call this when tx, ty, tz change

    Parameters:
        rx: hexapod x rotation
        ry: hexapod y rotation
        rz: hexapod z rotation
        tx: hexapod x translation
        ty: hexapod y translation
        tz: hexapod z translation
        corx: center of rotation x
        cory: center of rotation y
        corz: center of rotation z

    Returns:
        ux: user x translation
        uy: user y translation
        uz: user z translation
    '''

    # hexapod position
    trans = np.array( (pos['tx'], pos['ty'], pos['tz']) )

    # center of rotation
    cor = np.array( (pos['corx'], pos['cory'], pos['corz']) )

    # combined rotation matrix
    rot = rotz(pos['rz']) @ roty(pos['ry']) @ rotx(pos['rx'])

    user = cor + rot.T @ (trans - cor)

    return {
        'ux': user[0],
        'uy': user[1],
        'uz': user[2],
    }

##############################################################

def calc_to_real(pos: dict) -> dict:
    ''' calculate user coordinates
    from hexapod positions and center of rotation positions

    Call this when corx, cory, corz, rx, ry, rz, ux, uy, uz change

    Parameters:
        rx: hexapod x rotation
        ry: hexapod y rotation
        rz: hexapod z rotation
        ux: user x translation
        uy: user y translation
        uz: user z translation
        corx: center of rotation x
        cory: center of rotation y
        corz: center of rotation z

    Returns:
        tx: hexapod x translation
        ty: hexapod y translation
        tz: hexapod z translation
    '''

    # user position
    user = np.array( (pos['ux'], pos['uy'], pos['uz']))

    # center of rotation
    cor = np.array( (pos['corx'], pos['cory'], pos['corz']) )

    # combined rotation matrix
    rot = rotz(pos['rz']) @ roty(pos['ry']) @ rotx(pos['rx'])

    trans = cor + rot @ (user - cor)

    return {
        'tx': trans[0],
        'ty': trans[1],
        'tz': trans[2],
    }

##############################################################
