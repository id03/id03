import numpy as np
import matplotlib.pyplot as plt
from typing import NamedTuple
from center_of_rotation import rotx, roty, rotz, calc_from_real, calc_to_real

hexapod = np.array([
    (-100, -5),
    (-100, 5),
    (0, 5),
    (0, 40),
    (10, 40),
    (0, 60),
    (-10, 40),
    (0, 40),
    (0, 5),
    (100, 5),
    (100, -5),
    (-100, -5)
])

# Logic:
# (1) specify a center of rotation in laboratory coordinates
# (2) specify a point to be moved into the center of rotation
#     in platform coordinates
# (3) calculate the position of the hexapod (pose)
# (4) for a list of positions in platform coordinate,
#     calculate the laboratory coordinates

#################################################################
# the hexapod position

class Pose:
    def __init__(self,
                 tx: float = 0.,
                 ty: float = 0.,
                 tz: float = 0.,
                 rx: float = 0.,
                 ry: float = 0.,
                 rz: float = 0.,
                ):
        self.tx = tx
        self.ty = ty
        self.tz = tz
        self.rx = rx
        self.ry = ry
        self.rz = rz

    @property
    def translation(self) -> np.ndarray:
        return np.array( [self.tx, self.ty, self.tz] )

    @translation.setter
    def translation(self, trans: np.ndarray) -> None:
        self.tx, self.ty, self.tz = trans

    @property
    def rotation(self) -> np.ndarray:
        return rotz(self.rz) @ roty(self.ry) @ rotx(self.rx)

    @property
    def rot(self) -> np.ndarray:
        return np.array( [self.rx, self.ry, self.rz] )
    
    @rot.setter
    def rot(self, rot: np.ndarray) -> None:
        self.rx, self.ry, self.rz = rot

    def transform(self, obj: np.ndarray) -> np.ndarray:
        ''' Pose: position of the hexapod
        obj: object coordinate (x,y,z)
        '''
        return self.translation + self.rotation @ obj
    
    def correction(self, cor:np.ndarray):
        offset = cor - self.rotation() @ cor
        new_pose = Pose(self.tx + offset[0],
                        self.ty + offset[1],
                        self.tz + offset[2],
                        self.rx,
                        self.ry,
                        self.rz
                        )
        return new_pose

    def __str__(self):
        txt = "Pose("
        txt += f"tx={self.tx:7.3f},"
        txt += f"ty={self.ty:7.3f},"
        txt += f"tz={self.tz:7.3f},"
        txt += f"rx={self.rx:7.3f},"
        txt += f"ry={self.ry:7.3f},"
        txt += f"rz={self.rz:7.3f})"
        return txt

#################################################################

def transform(coords: np.ndarray, pose: Pose) -> np.ndarray:
    new_coords = np.zeros(shape=coords.shape)
    for i in range(coords.shape[0]):
        obj = np.array([0., coords[i, 0], coords[i,1]])

        plf = pose.transform(obj)
        new_coords[i, 0] = plf[1]
        new_coords[i, 1] = plf[2]
    return new_coords

#################################################################

def real_to_dict(pose: Pose, cor:np.ndarray) -> dict:
    ''' convert pose and cor arrays to dictionary
    translations of pose are hexapod (real) coordinates
    use with calc_from_real
    '''
    return {
        'corx': cor[0],
        'cory': cor[1],
        'corz': cor[2],
        'rx': pose.rx,
        'ry': pose.ry,
        'rz': pose.rz,
        'tx': pose.tx,
        'ty': pose.ty,
        'tz': pose.tz
    }        

#################################################################

def calc_to_dict(pose: Pose, cor:np.ndarray) -> np.ndarray:
    ''' convert pose and cor arrays to dictionary
    translatiosn of pose are user (calc) coordinates
    use with calc_to_real
    '''
    return {
        'corx': cor[0],
        'cory': cor[1],
        'corz': cor[2],
        'rx': pose.rx,
        'ry': pose.ry,
        'rz': pose.rz,
        'ux': pose.tx,
        'uy': pose.ty,
        'uz': pose.tz
    }

#################################################################

def dict_to_calc(pars: dict) -> np.ndarray:
    '''
    extract "calc" positions from dictionary
    use with calc_from_real
    '''
    # need to overwrite angles as these are not calculated
    return np.array( (pars['ux'],
                      pars['uy'],
                      pars['uz']) )

#################################################################

def dict_to_real(pars: dict) -> np.ndarray:
    '''
    extract "real" positions from dictionary
    use with calc_to_real
    '''
    # need to overwrite angles as these are not calculated
    return np.array( (pars['tx'],
                      pars['ty'],
                      pars['tz']) )

################################################################

def bliss_from_real(pose: Pose, cor: np.ndarray) -> Pose:
    '''
    use "calc_from_real" to calculate the user translations
    for a given center of rotation and hexapod position
    '''

    # create dictionary for Bliss function
    pars = real_to_dict(pose, cor)
    # Bliss calculation of "real" motor positions
    trans = calc_from_real(pars)
    # transform output dictionary to vector, update hexapod position
    trans = dict_to_calc(trans)
    
    return trans


################################################################

def bliss_correction(pose: Pose, cor: np.ndarray) -> Pose:
    '''
    use "calc_to_real" to calculate the hexapod
    position for a given center of rotation
    '''

    # create dictionary for Bliss function
    pars = calc_to_dict(pose, cor)
    # Bliss calculation of "real" motor positions
    trans = calc_to_real(pars)
    # transform output dictionary to vector, update hexapod position
    pose.translation = dict_to_real(trans)

    return pose

#################################################################

        
def main():
    fig, ax = plt.subplots()

    cor = (0., 0., 60.)
    #cor = (0., -10., 40.)
    tx = 0.
    ty = 10.
    tz = 20.
    coords = {}
    for i, chi in enumerate(range(-10, 15, 5)):
        pose = bliss_correction(Pose(tx, ty, tz, rx = chi), cor)
        trans = bliss_from_real(pose, cor)
        print(f"chi = {chi:7.3f}")
        print(f"-  input: {tx:7.3f}, {ty:7.3f}, {tz:7.3f}")
        print(f"- output: {trans[0]:7.3f}, {trans[1]:7.3f}, {trans[2]:7.3f}")
        coords[chi] = transform(hexapod, pose)
        
        ax.plot(coords[chi][:,0], coords[chi][:,1],
                label=f"{chi}", color=f'C{i}')
    ax.set_aspect('equal')
    ax.legend()
    ax.set_xlim(-110,110)
    ax.set_ylim(-30,110)
    plt.show()

if __name__ == "__main__":
    main()
