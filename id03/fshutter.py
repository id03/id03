import gevent

from bliss.config.settings import HashObjSetting

class FastShutter:
    def __init__(self, name, config):
        super().__init__()
        self.__name = name
        self.__config = config
        self.__shutter_mux = config["shutter_mux"]
        self.__settings = HashObjSetting(f"fastshutter:{name}")
        self.__time = None
        self.__enabled = self.__settings.get("enabled", True)
        if not self.__enabled:
            self.disable()

    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    def __info__(self):
        if self.is_enabled():
            scan_state = "YES"
        else:
            scan_state = "NO (shutter DISABLED)"
        info = f"FastShutter [{self.name}]:\n"
        info += f"Shutter mode          = {self.mode}\n"
        info += f"Shutter time          = {self.shutter_time:.3f} sec\n"
        info += f"Shutter used in scans = {scan_state}\n"
        return info

    @property
    def shutter_time(self):
        if self.__time is None:
            self.__time = self.__settings.get("time", 0.)
        return self.__time

    @shutter_time.setter
    def shutter_time(self, value):
        self.__settings["time"] = value
        self.__time = None

    @property
    def multiplexer(self):
        return self.__shutter_mux

    def enable(self):
        self.__enabled = True
        self.__settings["enabled"] = True

    def disable(self):
        self.__enabled = False
        self.__settings["enabled"] = False
        self.__shutter_mux.switch("SHUTTER", "CLOSED")

    def is_enabled(self):
        return self.__enabled
    
    def open(self, wait=False):
        #if not self.__enabled:
        #    raise RuntimeError("Shutter is DISABLED. Cannot open it !!")
        self.__shutter_mux.switch("SHUTTER", "OPEN")
        if wait:
            gevent.sleep(self.shutter_time)

    def close(self, wait=False):
        self.__shutter_mux.switch("SHUTTER", "CLOSED")
        if wait:
            gevent.sleep(self.shutter_time)

    @property
    def state(self):
        return self.__shutter_mux.getOutputStat("SHUTTER")

    @property
    def mode(self):
        return self.__shutter_mux.getOutputStat("SHUTTER")

    @mode.setter
    def mode(self, value):
        if not self.__enabled:
            raise RuntimeError("Shutter is DISABLED. Cannot change mode !!")
        self.__shutter_mux.switch("SHUTTER", value.upper())

    def is_open(self):
        return self.mode == "OPEN"

