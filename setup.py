# -*- coding: utf-8 -*-

"""The setup script."""

import sys
from setuptools import setup, find_packages

TESTING = any(x in sys.argv for x in ['test', 'pytest'])

with open('README.rst') as readme_file:
    readme = readme_file.read()

requirements = ['bliss']

setup_requirements = ['pytest-runner', 'pytest'] if TESTING else []

test_requirements = ['pytest-cov', 'mock']

setup(
    author="BCU Team",
    author_email='papillon@esrf.fr',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    description="ID03 bliss software",
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme,
    include_package_data=True,
    keywords='id03',
    name='id03',
    packages=find_packages(include=['id03']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.esrf.fr/id03/id03',
    version='0.1.0',
    zip_safe=False,
)
